import { Module } from '@nestjs/common';
import { StagesModule } from './modules/stages/stages.module';
import { StepsModule } from './modules/steps/steps.module';
import { StorageModule } from './modules/storage/storage.module';

@Module({
  imports: [StagesModule, StepsModule, StorageModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
