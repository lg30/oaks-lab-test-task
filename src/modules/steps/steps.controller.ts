import { Controller, Post, Body, Patch, Param } from '@nestjs/common';
import { StepsService } from './steps.service';
import {
  ICreateStepArgs,
  IStep,
  IUpdateStepParams,
  IUpdateStepArgs,
} from './steps.types';

@Controller('steps')
export class StepsController {
  constructor(private readonly steps_svc: StepsService) {}

  @Post()
  create(@Body() args: ICreateStepArgs): IStep {
    return this.steps_svc.create(args);
  }

  @Patch(':stageId/:stepId')
  update(
    @Param() params: IUpdateStepParams,
    @Body() args: IUpdateStepArgs,
  ): IStep {
    return this.steps_svc.update(
      {
        stepId: Number(params.stepId),
        stageId: Number(params.stageId),
      },
      args,
    );
  }
}
