import { Inject, Injectable } from '@nestjs/common';
import { StorageService } from '../storage/storage.service';
import {
  ICreateStepArgs,
  IStep,
  IUpdateStepArgs,
  IUpdateStepParams,
} from './steps.types';

@Injectable()
export class StepsService {
  constructor(
    @Inject(StorageService) private readonly storage_svc: StorageService,
  ) {}

  create(args: ICreateStepArgs): IStep {
    return this.storage_svc.createStep(args);
  }

  update(params: IUpdateStepParams, args: IUpdateStepArgs): IStep {
    return this.storage_svc.updateStep(params, args);
  }
}
