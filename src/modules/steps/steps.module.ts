import { Module } from '@nestjs/common';
import { StepsService } from './steps.service';
import { StepsController } from './steps.controller';
import { StorageService } from '../storage/storage.service';

@Module({
  providers: [StepsService, StorageService],
  controllers: [StepsController],
})
export class StepsModule {}
