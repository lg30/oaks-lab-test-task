export interface IStep {
  id: number;
  title: string;
  checked: boolean;
}

export interface ICreateStepArgs {
  title: string;
  stageId: number;
}

export interface IUpdateStepArgs {
  title?: string;
  checked?: boolean;
}

export interface IUpdateStepParams {
  stepId: number;
  stageId: number;
}
