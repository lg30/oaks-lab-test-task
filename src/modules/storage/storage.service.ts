import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ICreateStageArgs, IStage } from '../stages/stages.types';
import {
  ICreateStepArgs,
  IStep,
  IUpdateStepArgs,
  IUpdateStepParams,
} from '../steps/steps.types';

@Injectable()
export class StorageService {
  static data: Array<IStage> = [];

  getStages(): Array<IStage> {
    return StorageService.data;
  }

  getStageById(id: number): IStage {
    const stage: undefined | IStage = StorageService.data.find(
      (item) => item.id === id,
    );
    if (!stage) {
      throw new HttpException(
        'Stage with given ID is not found',
        HttpStatus.NOT_FOUND,
      );
    }
    return stage;
  }

  createStage(args: ICreateStageArgs): IStage {
    const { title } = args;
    // this ID is for memory storage
    const id: number = StorageService.data.length;
    // we cast to Boolean to have true or false in response
    const locked = !!(
      StorageService.data.length &&
      !StorageService.data[StorageService.data.length - 1].done
    );
    StorageService.data.push({
      id,
      title,
      steps: [],
      locked,
      // created stage is done only if it's not the first element, and it's unlocked (the previous stage is done)
      done: !!(StorageService.data.length && !locked),
    });
    return StorageService.data[id];
  }

  createStep(args: ICreateStepArgs): IStep {
    const { stageId, title } = args;
    const stage: undefined | IStage = StorageService.data.find((item) => {
      return item.id === stageId;
    });
    if (!stage) {
      throw new HttpException(
        'Stage with given ID is not found',
        HttpStatus.NOT_FOUND,
      );
    }
    const id: number = stage.steps.length;
    stage.steps.push({
      id,
      title,
      checked: false,
    });
    return stage.steps[id];
  }

  updateStep(params: IUpdateStepParams, args: IUpdateStepArgs): IStep {
    const { title, checked } = args;
    const { stepId, stageId } = params;

    const stage: undefined | IStage = StorageService.data.find(
      (item) => item.id === stageId,
    );
    if (!stage) {
      throw new HttpException(
        'Stage with given ID is not found',
        HttpStatus.NOT_FOUND,
      );
    }

    const step: undefined | IStep = stage.steps.find(
      (item) => item.id === stepId,
    );
    if (!step) {
      throw new HttpException(
        'Step with given ID is not found',
        HttpStatus.NOT_FOUND,
      );
    }

    if (title !== undefined) {
      step.title = title;
    }

    if (checked !== undefined) {
      // if stage is locked, you can't change status of its step
      if (stage.locked) {
        throw new HttpException(
          'Stage with given ID is locked',
          HttpStatus.FORBIDDEN,
        );
      }

      // you can't change status of step if stage is done and next stage is already started (some step is checked)
      const nextStage = StorageService.data.find(
        (item) => item.id === stage.id + 1,
      );

      // stage is completely done if it's done and some item from next stage is already checked
      if (
        stage.done &&
        nextStage &&
        nextStage.steps.some((item) => item.checked)
      ) {
        throw new HttpException(
          'Stage with given ID is done',
          HttpStatus.FORBIDDEN,
        );
      }

      step.checked = checked;

      // update current stage done status and lock status of next stage based on that
      stage.done = stage.steps.every((item) => item.checked);
      if (nextStage) {
        nextStage.locked = !stage.done;
      }
    }
    return step;
  }
}
