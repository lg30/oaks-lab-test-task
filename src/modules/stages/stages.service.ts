import { Inject, Injectable } from '@nestjs/common';
import { ICreateStageArgs, IStage } from './stages.types';
import { StorageService } from '../storage/storage.service';

@Injectable()
export class StagesService {
  constructor(
    @Inject(StorageService) private readonly storage_svc: StorageService,
  ) {}

  get(): Array<IStage> {
    return this.storage_svc.getStages();
  }

  getById(id: number): IStage {
    return this.storage_svc.getStageById(id);
  }

  create(args: ICreateStageArgs): IStage {
    return this.storage_svc.createStage(args);
  }
}
