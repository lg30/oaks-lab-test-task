import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { StagesService } from './stages.service';
import { ICreateStageArgs, IGetByIdParams, IStage } from './stages.types';

@Controller('stages')
export class StagesController {
  constructor(private readonly stages_svc: StagesService) {}

  @Get()
  get(): Array<IStage> {
    return this.stages_svc.get();
  }

  @Get(':id')
  getById(@Param() params: IGetByIdParams): IStage {
    return this.stages_svc.getById(Number(params.id));
  }

  @Post()
  create(@Body() args: ICreateStageArgs): IStage {
    return this.stages_svc.create(args);
  }
}
