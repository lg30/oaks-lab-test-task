import { Module } from '@nestjs/common';
import { StagesService } from './stages.service';
import { StagesController } from './stages.controller';
import { StorageService } from '../storage/storage.service';

@Module({
  providers: [StagesService, StorageService],
  controllers: [StagesController],
})
export class StagesModule {}
