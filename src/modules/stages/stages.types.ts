import { IStep } from '../steps/steps.types';

export interface IStage {
  id: number;
  title: string;
  steps: Array<IStep>;
  locked: boolean;
  done: boolean;
}

export interface ICreateStageArgs {
  title: string;
}

export interface IGetByIdParams {
  id: number;
}
